<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Usuario_model extends MY_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function valida_usuario($params =  []){
        $salida = array(
            'status' => false,
            'msg' => 'Usuario incorrecto'
        );
        $filtros = array(
            'select' => array(
                'ID', 'FirstName', 'LastName', 'Email', 'Password'
            ),
            'where' => array(
                'Email' => $params['username']
            )
        );
        $usuario = $this->get_registros('Users', $filtros);
        if(!is_null($usuario) && count($usuario) > 0 && $usuario[0]['Password'] == md5($params['userpass'])){
            $salida['status'] = true;
            unset($usuario[0]['Password']);
            $salida['informacion_usuario'] = $usuario[0];
        }else{
            // pr($this->db->last_query());
            // pr($usuario);
            // pr(md5($params['userpass']));
        }
        return $salida;
    }

}
