<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Construccion_model extends MY_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function actualiza_galeria($clave_construccion = null, $galeria){
        $where = array('clave' => $clave_construccion);
        $this->delete_registros('galeria', $where);

        foreach ($galeria as $i) {
            $i['clave'] = $clave_construccion;
            $this->insert_registro('galeria', $i, false);
        }
    }

}
