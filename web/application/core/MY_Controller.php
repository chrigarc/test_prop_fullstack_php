<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @author: Chrigarc
 * @version: 1.0
 * @desc: Clase padre de los controladores del sistema
 * */
class MY_Controller extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->lang->load('interface', 'spanish');
    }
    /**
    *
    **/
    public function prepara_filtros($array = [])
    {
        $salida = array();
        $omitidos = array('pageIndex', 'pageSize', 'sortField', 'sortOrder');
        foreach ($array as $key => $value)
        {
            if(array_search($key, $omitidos) === FALSE && !is_null($array[$key]) && $array[$key] != '')
            {
                $salida[$key] = $value;
            }
        }
        return $salida;
    }
}
