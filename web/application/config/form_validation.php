<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$config['login_form'] = array(
    array(
       'field' => 'username',
       'label' => 'Correo electrónico',
       'rules' => 'trim|required|valid_email'
   ),
   array(
       'field' => 'userpass',
       'label' => 'Contraseña',
       'rules' => 'trim|required'
   )
);

$config['insert_form'] = array(
    array(
        'field' => 'clave',
        'label' => 'Clave',
        'rules' => 'trim|required'
    ),
    array(
        'field' => 'nombre',
        'label' => 'Nombre',
        'rules' => 'trim|required'
    ),
    array(
        'field' => 'estado',
        'label' => 'Estado',
        'rules' => 'trim|required'
    ),
    array(
        'field' => 'municipio',
        'label' => 'Municipio',
        'rules' => 'trim|required'
    ),
    array(
        'field' => 'colonia',
        'label' => 'Colonia',
        'rules' => 'trim|required'
    ),
    array(
        'field' => 'calle',
        'label' => 'Calle',
        'rules' => 'trim|required'
    ),
    array(
        'field' => 'numero',
        'label' => 'Número',
        'rules' => 'trim|required'
    ),
    array(
        'field' => 'latitud',
        'label' => 'Latitud',
        'rules' => 'trim|required|is_numeric'
    ),
    array(
        'field' => 'longitud',
        'label' => 'Longitud',
        'rules' => 'trim|required|is_numeric'
    ),
);
