<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Construccion extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('Construccion_model', 'construccion');
    }

    public function lista(){
        $output = [];
        $output = array('data' => [], 'itemsCount' => 0);
        $filtros = [];
        $params = $this->input->get(null, true);
        $filtros['limit'] = isset($params['pageSize'])? $params['pageSize']:Catalogo::LIMIT;
        $filtros['offset'] = isset($params['pageIndex'])?  ($filtros['limit']*($params['pageIndex']-1)):0;
        $filtros['like'] = $this->prepara_filtros($params);
        $output['data'] = $this->construccion->get_registros('construcciones', $filtros);
        $filtros['total'] = true;
        $output['itemsCount'] = $this->construccion->get_registros('construcciones', $filtros);
        header('Content-Type: application/json; charset=utf-8;');
        echo json_encode($output);
    }

    public function insertar(){
        $output = array('success' => false, 'message' => 'Información incorrecta', 'data'=>[]);
        if($this->input->post()){
            $post = $this->input->post(null, true);
            $this->config->load('form_validation'); //Cargar archivo con validaciones
            $validations = $this->config->item('insert_form'); //Obtener validaciones de archivo general
            $this->form_validation->set_rules($validations); //Añadir validaciones
            $output['data'] = $post;
            if ($this->form_validation->run() == TRUE)
            {
                $output = $this->construccion->insert_registro('construcciones', $post, false);
                $this->crear_galeria($post['clave'], $post['latitud'], $post['longitud'], false);
            }else{
                $output['success'] = false;
                $output['form_errors'] = validation_errors();
            }
            header('Content-Type: application/json; charset=utf-8;');
            echo json_encode($output);
        }
    }

    public function crear_galeria($clave = null, $latitud = null, $longitud = null, $echo = true){
        try{
            $this->config->load('foursquare');
            $keys = $this->config->item('foursquare');
            $date = date('Ymd');
            $url = 'https://api.foursquare.com/v2/venues/';
    		$url .= 'explore?';
    		$url .= "client_id={$keys['client_id']}";
    		$url .= "&client_secret={$keys['client_secret']}";
            $url .= "&v={$date}";
            $url .= "&ll={$latitud},{$longitud}";
            $url .= '&query=build';
            $url .= '&limit=5';
    		$json = file_get_contents($url);
    		$data = json_decode($json, TRUE);
            // pr($data);
            $elementos = [];
            if(isset($data['response']['groups'][0]['items'])){
                foreach ($data['response']['groups'][0]['items'] as $item) {
                    $id = $item['venue']['id'];
                    $foto = $this->get_photo($id, $keys);
                    $name = $item['venue']['name'];
                    if(!is_null($foto) && $foto!=''){
                        $elementos[]=array('id_imagen' => $id, 'name' => $name, 'foto' =>$foto);
                    }
                }
                // pr($elementos);
            }
            $this->construccion->actualiza_galeria($clave, $elementos);
            //pr($data);
            if($echo){
                header('Content-Type: application/json; charset=utf-8;');
                echo json_encode($elementos);
            }
        }catch(Exception $ex){

        }
    }

    public function galeria($clave, $aux){
        $filtros = array(
            'where' => array(
                'clave' => $clave.'/'.$aux
            )
        );
        $output['galeria'] = $this->construccion->get_registros('galeria', $filtros);
        $this->load->view('construccion/galeria.tpl.php', $output);
    }

    public function eliminar(){
        if($this->input->post()){
            $post = $this->input->post(null, true);
            $where = array('clave' => $post['clave']);
            $this->construccion->delete_registros('galeria', $where);
            $output =  $this->construccion->delete_registros('construcciones', $where);
            header('Content-Type: application/json; charset=utf-8;');
            echo json_encode($output);
        }
    }

    public function actualizar(){
        if($this->input->post()){
            $post = $this->input->post(null, true);
            $where = array('clave' => $post['clave']);
            $output = $this->construccion->update_registro('construcciones', $post, $where);
            $output['data'] = $post;
            if($output['success']){
                $this->crear_galeria($post['clave'], $post['latitud'], $post['longitud'], false);
            }
            header('Content-Type: application/json; charset=utf-8;');
            echo json_encode($output);
        }
    }

    private function get_photo($id_venue, &$keys){
        $photo = null;
        $date = date('Ymd');
        $url = 'https://api.foursquare.com/v2/venues/'.$id_venue;
		$url .= '/photos?';
		$url .= "client_id={$keys['client_id']}";
		$url .= "&client_secret={$keys['client_secret']}";
        $url .= "&v={$date}";
        $json = file_get_contents($url);
        $data = json_decode($json, TRUE);
        if(isset($data['response']['photos']['items'][0]['prefix'])){
            $photo = $data['response']['photos']['items'][0]['prefix']."300x300".$data['response']['photos']['items'][0]['suffix'];
        }
        return $photo;
    }

    public function actualiza_caracteristicas(){
        if($this->input->post()){
            $post = $this->input->post(null, true);
            $params = array('caracteristicas' => $post['caracteristicas_guardadas']);
            $where = array('clave' =>$post['clave']);
            $output = $this->construccion->update_registro('construcciones', $params, $where);
            $filtros = array(
                'where' => array(
                    'clave' => $post['clave']
                )
            );
            $output['tipo'] = 1;
            $output['construcciones'] = $this->construccion->get_registros('construcciones', $filtros);
            $this->load->view('construccion/caracteristicas.tpl.php', $output);
        }
    }

    public function caracteristicas($clave, $aux, $t = 1){
        $output['tipo'] = $t; //para activar o desactivar el boton de quitar de la vista
        $filtros = array(
            'where' => array(
                'clave' => $clave.'/'.$aux
            )
        );
        $output['construcciones'] = $this->construccion->get_registros('construcciones', $filtros);
        $this->load->view('construccion/caracteristicas.tpl.php', $output);
    }

}
