<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inicio extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_complete');
        $this->load->library('form_validation');
    }

    public function index(){
        $output = [];
        $output['texts'] = $this->lang->line('login_form');

        if($this->input->post()){
            $this->load->model('Usuario_model', 'usuario');
            $post = $this->input->post(null, true);
            $this->config->load('form_validation'); //Cargar archivo con validaciones
            $validations = $this->config->item('login_form'); //Obtener validaciones de archivo general
            $this->form_validation->set_rules($validations); //Añadir validaciones
            $output = array_merge($output, $post);
            if ($this->form_validation->run() == TRUE)
            {
                $output['result'] = $this->usuario->valida_usuario($post);
                if($output['result']['status']){
                    $this->session->set_userdata('sistema', $output['result']['informacion_usuario']);
                }
            }else{
                $output['result']['status'] = false;
                $output['form_errors'] = validation_errors();
            }
            $output['html'] = $this->load->view('inicio/index.tpl.php', $output, true);
            header('Content-Type: application/json; charset=utf-8;');
            echo json_encode($output);
        }else{
            $output['incluir_script'] = true;
            $view = $this->load->view('inicio/index.tpl.php', $output, true);
            $this->template->setMainContent($view);
            $this->template->getTemplate(false, 'tc_template/index.tpl.php');
        }
    }

    public function principal(){
        $output = [];
        $view = $this->load->view('inicio/principal.tpl.php', $output, true);
        $this->template->setMainContent($view);
        $this->template->setCSSFiles(array(
            'https://cdnjs.cloudflare.com/ajax/libs/jsgrid/1.5.3/jsgrid.min.css',
            'https://cdnjs.cloudflare.com/ajax/libs/jsgrid/1.5.3/jsgrid-theme.min.css'
        ));
        $this->template->getTemplate(false, 'tc_template/index.tpl.php');
    }

    public function cerrar_sesion(){
        $this->session->sess_destroy();
        redirect(site_url());
    }

}
