<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Clase que contiene métodos para la carga de la plantilla base del sistema y creación de la paginación
 * @version 	: 1.1.0
 * @author 	: LEAS
 * @property    : mixed[] Data arreglo de datos de plantilla con la siguisnte estructura array("title"=>null,"nav"=>null,"main_title"=>null,"main_content"=>null);
 * */
class Template {

    private $elements;
    var $lang;
    var $lang_text;
    var $multiligual;
    private $datos_imagen_perfil;

    public function __construct() {
        $this->CI = & get_instance();
        $this->CI->load->helper('html');
        $this->lang = "spanish";
        $this->new_tpl = FALSE;
        $this->lang_text = array();
        $this->elements = array(
            "title" => null, //Titulo de de la sección
            "menu" => null, //Pinta todo el cuerpo del menú en el template
            "main_title" => null, //Pinta un titulo, el titulo del "main_content", puede o no existir
            "sub_title" => null, //Pinta un subtitulo, el sub_titulo del "main_content", puede o no existir
            "descripcion" => null, //
            "main_content" => null, //contenido del template, información relevante de la sección en cuestion
            "css_files" => null, //Carga rutas de archivos css que se llamarán al pintar el formulario de core
            "js_files" => null, //Carga rutas de archivos js que se llmaran al pintar el formulario de core
            "css_script" => null,
            "cuerpo_modal" => null,
            "blank" => null,
        );
    }


    function getElements() {
        return $this->elements;
    }

    /* regresa en pantalla el contenido de la plantilla
     * @method: array getData()
     * @return: mixed[] Data arreglo de datos de plantilla con la siguisnte estructura array("title"=>null,"nav"=>null,"main_title"=>null,"main_content"=>null);
     */
    function getTemplate($tipo = FALSE, $tpl = 'tc_template/principal.tpl.php') {
        if ($this->multiligual) {
            $this->CI->lang->load('interface', $this->lang);
            $this->elements["string_tpl"] = $this->CI->lang->line('interface_tpl');
            // pr($this->elements);
        }
        if ($tipo) {
            $this->CI->load->view($tpl, $this->elements, TRUE);
        }
        $this->CI->load->view($tpl, $this->elements);
    }

    /**
     * Método que carga datos en la plantilla base del sistema
     * @author 		: Jesús Díaz P.
     * @modified 	: Miguel Guagnelli
     * @access 		: public
     * @method:     : void
     * @param 		: mixed[] $elements Elementos configurables en la plantilla
     */
    public function setTemplate($elements = array()) {
        $this->elements['title'] = (array_key_exists('title', $elements)) ? $elements['title'] : null;
        $this->elements['menu'] = $this->templete_menu(); //(array_key_exists('menu', $elements)) ? $elements['menu'] : null;
        $this->elements['main_title'] = (array_key_exists('main_title', $elements)) ? $elements['main_title'] : null;
        $this->elements['sub_title'] = (array_key_exists('sub_title', $elements)) ? $elements['sub_title'] : null;
        $this->elements['descipcion'] = (array_key_exists('descipcion', $elements)) ? $elements['descipcion'] : null;
        $this->elements['blank'] = (array_key_exists('blank', $elements)) ? $elements['blank'] : null;
        $this->elements['main_content'] = (array_key_exists('main_content', $elements)) ? $elements['main_content'] : null;
        $this->elements['css_files'] = (array_key_exists('css_files', $elements)) ? $elements['css_files'] : null;
        $this->elements['js_files'] = (array_key_exists('js_files', $elements)) ? $elements['js_files'] : null;
        $this->elements['css_script'] = (array_key_exists('css_script', $elements)) ? $elements['css_script'] : null;
        $this->elements['myModal'] = (array_key_exists('myModal', $elements)) ? $elements['myModal'] : null;
    }

    /*
     * Asigna valores a la propiedad Titulo de la plantilla
     * @author  : Miguel Guagnelli
     * @method  : void setTitle($title)
     * @access  : public
     * @param   : string $title Es el título de la pestaña de la plantilla.
     */

    function setTitle($title = null) {
        $this->elements["title"] = $title;
    }

    /*
     * Asigna la propiedad de opciones de menú de la plantilla
     * @author  : CAQZ
     * @method  : void setNav($nav)
     * @access  : public
     * @param   : mixed[] $nav Arreglo compuesto de n elementos con la sig estructura array("link"=>"","titulo"=>"","attribs"=>array())",
     */

    function setNav($menu = NULL) {
        //$vista_menu = $this->CI->load->view('tc_template/menu.tpl.php', $menu, TRUE);

        //$this->elements["menu"] = $vista_menu;
        $this->elements["menu"] = $menu;
    }

    /*
     * Asigna la propiedad de título de la sección en la plantilla
     * @author  : Miguel Guagnelli
     * @method: void setMainTitle($main_title)
     * @param: string $main_title Titulo de la sección en la que se encuentra el usuario
     */

    function setMainTitle($main_title = null) {
        $this->elements["main_title"] = $main_title;
    }

    /*
     * Asigna la propiedad de contenido principal en la plantilla
     * @author  : Miguel Guagnelli
     * @method: void setMainContent($main_content)
     * @param: string $main_content Contenido principal de la plantill
     */

    function setMainContent($main_content = null, $options = null, $isProcesed = true) {
        if ($isProcesed) {
            $this->elements["main_content"] = $main_content;
            return true;
        }

        if (!is_null($main_content)) {
            $this->elements["main_content"] = $this->CI->load->view($main_content, $options, true);
            return true;
        } else {
            return false;
        }
    }

    function setCSSFiles($css_files){
        $this->elements["css_files"] = $css_files;
    }
}
