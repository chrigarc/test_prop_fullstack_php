<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <?php
            $index = 0;
            foreach ($galeria as $item) {
        ?>
        <li data-target="#myCarousel" data-slide-to="<?php echo $index; ?>" class="<?php echo ($index++ == 0)?'active':''; ?>"></li>
        <?php
            }
        ?>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
        <?php
            $index = 0;
            foreach ($galeria as $item) {
        ?>
        <div class="item <?php echo ($index++ == 0)?'active':''; ?>">
            <img src="<?php echo $item['foto']; ?>" alt="<?php echo $item['name'];?>" style="width:100%;">
        </div>
        <?php
            }
        ?>
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right"></span>
        <span class="sr-only">Next</span>
    </a>
</div>
