<?php
    if(isset($success)){
        $t = $success?'success':'danget';
        echo html_message($message, $t);
    }

    if(count($construcciones) > 0){
        // pr($construcciones);
        $caracteristicas = json_decode($construcciones[0]['caracteristicas'], true);
        if($caracteristicas){
            foreach ($caracteristicas as $item) {
                ?>
                <div class="col-md-4">
                    <?php echo $tipo == 1 ?'<i class="fas fa-times-circle" onclick="quitar_caracteristica(this)"></i>':''; ?>
                    <p class="item-caracteristica">
                        <?php echo $item; ?>
                    </p>
                </div>
                <?php
            }
        }
        ?>
            <input type="hidden" name="clave" value="<?php echo $construcciones[0]['clave']; ?>">
        <?php
    }
?>
