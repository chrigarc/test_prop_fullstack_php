<?php
if(isset($incluir_script) && $incluir_script){
    echo js('inicio/login.js');
}
?>
<div class="" id="area_form">


    <form id="login_form" class="form-horizontal" method="post">
        <fieldset>

            <!-- Form Name -->
            <legend><?php echo $texts['inicio_sesion']?></legend>

            <!-- Text input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="textinput"><?php echo $texts['username']; ?></label>
                <div class="col-md-4">
                    <?php
                    echo $this->form_complete->create_element(array('id' => 'username',
                    'type' => 'text',
                    'value' => isset($post['username'])?$post['username']:'',
                    'attributes' => array(
                        'class' => 'form-control input-md',
                        'required'=>true,
                        'placeholder' =>$texts['username']
                    )));
                    echo form_error_format('username');
                    ?>
                </div>
            </div>

            <!-- Password input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="passwordinput"><?php echo $texts['userpass']; ?></label>
                <div class="col-md-4">
                    <?php
                    echo $this->form_complete->create_element(array('id' => 'userpass',
                    'type' => 'password',
                    'value' => isset($post['userpass'])?$post['userpass']:'',
                    'attributes' => array(
                        'class' => 'form-control input-md',
                        'required'=>true,
                        'placeholder' =>$texts['userpass']
                    )));
                    echo form_error_format('userpass');
                    ?>
                </div>
            </div>

            <!-- Button -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="singlebutton"></label>
                <div class="col-md-4">
                    <button id="singlebutton" name="singlebutton" class="btn btn-primary"><?php echo $texts['iniciar_sesion']; ?></button>
                </div>
            </div>

        </fieldset>
    </form>
</div>
