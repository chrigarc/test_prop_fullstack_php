<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jsgrid/1.5.3/jsgrid.min.js"></script>
<?php echo js('inicio/principal.js'); ?>
<script async defer
src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDlZUa_m7WSboz-KIlb4MGTgvlrdobMXVs&callback=initMap">
</script>
<div class="row">
    <div class="col col-md-12">
        <div class="pager-panel">
            <label>Cantidad por pagina:
                <select id="pager">
                    <option>5</option>
                    <option>10</option>
                    <option selected>25</option>
                    <option>50</option>
                    <option>100</option>
                    <option>200</option>
                </select>
            </label>
        </div>
        <div id="jsGrid"></div>
    </div>
</div>
<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myLargeModalLabel"></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <p>
                            <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                                Mostrar características
                            </a>
                        </p>
                        <div class="collapse" id="collapseExample">
                            <div class="card card-body">
                                <div id="area_caracteristicas2"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div id="map"></div>
                    </div>
                    <!-- <div class="col col-md-6" id="galeria">

                    </div> -->
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade bd-caracteristicas-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">Características para <span id="carac_clave"></span></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <form id="caracteristica-form" class="" action="<?php echo site_url("construcciones/caracteristicas"); ?>" method="post">
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="textinput">Nueva característica</label>
                            <div class="col-md-4">
                                <input id="textinput" name="textinput" placeholder="Nueva característica" class="form-control input-md" type="text">
                            </div>
                            <div class="col-md-4">
                                <a id="singlebutton" onclick="agregar_caracteristica()" class="btn btn-primary">Agregar</a>
                            </div>
                        </div>
                        <br>
                        <div id="area_caracteristicas" class="col-md-12">
                        </div>
                        <input type="hidden" id="caracteristicas_guardadas" name="caracteristicas_guardadas" value="">
                        <div class="form-group">
                            <div class="col-md-12">
                                <button id="singlebutton" name="singlebutton" class="btn btn-primary">Guardar</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
