<!DOCTYPE html>
<html lang="en">
<head>
    <title>Inmobiliaria Propiedades</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Examen Práctico: Software Engineer">
    <meta name="author" content="Propiedades.com">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.12/css/all.css" integrity="sha384-G0fIWCsCzJIMAVNQPfjH08cyYaUtMwjJwqiRKxxE/rx96Uroj1BtIQ6MLJuheaO9" crossorigin="anonymous">
    <?php echo css('style.css'); ?>
    <?php if(isset($css_files)){
        foreach ($css_files as $item) {
            ?>
            <link rel="stylesheet" href="<?php echo $item; ?>">
            <?php
        }
    }
    ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script type="text/javascript">
        var url = "<?php echo base_url(); ?>";
        var site_url = "<?php echo site_url(); ?>";
        var img_url_loader = "<?php echo base_url('assets/img/loader.gif'); ?>";
    </script>
    <?php echo js('general.js'); ?>
</head>
<body>

    <div id="overlay">
         <img src="<?php echo base_url('assets/img/loader.gif'); ?>" alt="Loading" /><br/>
         Cargando...
    </div>

    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Logo</a>
            </div>
            <div class="collapse navbar-collapse" id="myNavbar">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="#">Inicio</a></li>
                </ul>
                <?php
                if(!is_null($this->session->userdata('sistema')) && isset($this->session->userdata('sistema')['ID'])) {
                    ?>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="<?php echo site_url('inicio/cerrar_sesion'); ?>"><span class="glyphicon glyphicon-log-in"></span> Cerrar sesión</a></li>
                    </ul>
                    <?php
                }
                ?>
            </div>
        </div>
    </nav>

    <div class="container-fluid text-center">
        <div class="row content">
            <div class="col-sm-2 sidenav">

            </div>
            <div class="col-sm-8 text-left">
                <h1>Examen Práctico: Software Engineer</h1>
                
                    <?php
                    if (isset($main_content)){
                        echo $main_content;
                    }
                    ?>

            </div>
            <div class="col-sm-2 sidenav">

            </div>
        </div>
    </div>

    <footer class="container-fluid text-center">
        <p>Test Propiedades.com</p>
    </footer>

</body>
</html>
