<?php

/*
 * Cuando escribí esto sólo Dios y yo sabíamos lo que hace.
 * Ahora, sólo Dios sabe.
 * Lo siento.
 */
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *
 * @author chrigarc
 */
class Loader
{

    function load()
    {
        $CI = & get_instance(); //Obtiene la insatancia del super objeto en codeigniter para su uso directo

        $CI->load->helper('url');
        $CI->load->library('session');

        if(!is_null($CI->session->userdata('sistema')) && isset($CI->session->userdata('sistema')['ID']) && $this->libre_acceso($CI)){
            redirect('inicio/principal');
        }else if(is_null($CI->session->userdata('sistema')) && !$this->libre_acceso($CI)){
            redirect('inicio/index');
        }

    }

    private function libre_acceso($CI){
        $status = false;
        $status = $CI->uri->segment(1) == 'inicio' && ($CI->uri->segment(2) == 'index' || $CI->uri->segment(2) == '');
        return $status;
    }
}
