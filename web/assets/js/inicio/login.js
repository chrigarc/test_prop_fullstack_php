$(function(){
    activa_login_form();
});

function callback_login(params){
    activa_login_form();
    console.log(params.object.result.status);
    if(params.object.result.status){
        window.location = site_url + '/inicio/principal';
    }
}

function activa_login_form(){
    $('#login_form').submit(function(event){
        event.preventDefault();
        var destino = site_url + '/inicio/index';
        data_ajax(destino, '#login_form','#area_form', callback_login, true,{});
    });
}
