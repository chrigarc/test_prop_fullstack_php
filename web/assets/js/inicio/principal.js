$(function(){
    render_grid();
    $('#caracteristica-form').submit(function(event){
        event.preventDefault();
        var destino = site_url + '/construccion/actualiza_caracteristicas';
        var arreglo = $('.item-caracteristica');
        var items = [];
        for(i = 0;i<arreglo.length;i++){
            items.push(arreglo[i].innerText);
        }
        items = JSON.stringify(items);
        console.log(items);
        $('#caracteristicas_guardadas').val(items);
        data_ajax(destino, '#caracteristica-form', '#area_caracteristicas');
    });
});

function render_grid(){

    jsGrid.validators.clave_valida = {
        message: "Por favor introduce una clave con el siguiente formato: PCOM-XXX/##",
        validator: function(value, item) {
            return /^PCOM-[A-Z]{3}[/][0-9]{2}/.test(value);
        }
    };

    var grid = $("#jsGrid").jsGrid({
        height: "500px",
        width: "100%",
        filtering: true,
        inserting: true,
        editing: true,
        sorting: true,
        selecting: false,
        onItemUpdating: function (args) {
            grid._lastPrevItemUpdate = args.previousItem;
        },
        controller: {
            loadData: function(filter) {
                var d = $.Deferred();
                mostrar_loader();
                $.ajax({
                    url: site_url + "/construccion/lista",
                    dataType: "json",
                    data: filter,
                }).done(function(response) {
                    d.resolve(response);
                }).always(function(){
                    ocultar_loader();
                });
                return d.promise();
            },
            insertItem: function (item){
                mostrar_loader();
                var di = $.Deferred();
                var direccion = item.calle + " " + item.numero + " " + 'colonia ' + item.colonia + ', ' + item.munucipio + ", " + item.estado;
                console.log(direccion);
                getLatLong(direccion).then(function(lat_long){
                    item.latitud = lat_long.lat();
                    item.longitud = lat_long.lng();
                    $.ajax({
                        type: "POST",
                        url: site_url + "/construccion/insertar",
                        data: item
                    }).done(function (json) {
                        alert(json.message + "<br>" + json.form_errors);
                        grid.insertSuccess = json.success;
                        di.resolve(json.data);
                    }).fail(function (jqXHR, error, errorThrown) {
                        console.log("error");
                        console.log(jqXHR);
                        console.log(error);
                        console.log(errorThrown);
                    }).always(function(){
                        ocultar_loader();
                    });
                });
                return di.promise();
            },
            deleteItem: function(item){
                mostrar_loader();
                return $.ajax({
                    type: "POST",
                    url: site_url + "/construccion/eliminar",
                    data: item
                }).always(function(){
                    ocultar_loader();
                });
            },
            updateItem: function (item) {
                mostrar_loader();
                var di = $.Deferred();
                var direccion = item.calle + " " + item.numero + " " + 'colonia ' + item.colonia + ', ' + item.munucipio + ", " + item.estado;
                console.log(direccion);
                getLatLong(direccion).then(function(lat_long){
                    item.latitud = lat_long.lat();
                    item.longitud = lat_long.lng();
                    $.ajax({
                        type: "POST",
                        url: site_url + "/construccion/actualizar",
                        data: item
                    }).done(function (json) {
                        alert(json.message + "<br>" + json.form_errors);
                        if (json.success) {
                            di.resolve(json.data);
                        } else {
                            di.resolve(grid._lastPrevItemUpdate);
                        }
                    }).fail(function (jqXHR, error, errorThrown) {
                        console.log("error");
                        console.log(jqXHR);
                        console.log(error);
                        console.log(errorThrown);
                    });
                    ocultar_loader();
                });
                return di.promise();
            }
        },
        paging: true,
        pageLoading: true,
        autoload: true,
        pageSize: 10,
        pageButtonCount: 3,
        pagerFormat: "Paginas: {first} {prev} {pages} {next} {last}    {pageIndex} de {pageCount}",
        pagePrevText: "Anterior",
        pageNextText: "Siguiente",
        pageFirstText: "Primero",
        pageLastText: "Último",
        pageNavigatorNextText: "...",
        pageNavigatorPrevText: "...",
        noDataContent: "No se encontraron datos",
        rowDoubleClick: function (value) {},
        rowClick: function(args) { },
        fields: [
            {name: 'clave', title: "Clave", type: 'text', inserting: true,editing: false, validate: [
            "required",
                { validator: "clave_valida", param: []}
            ]},
            {name: 'nombre', title: 'Nombre', type: 'text', validate: 'required'},
            {name: 'estado', title: 'Estado', type: 'text', validate: 'required'},
            {name: 'municipio', title: 'Municipio', type: 'text', validate: 'required'},
            {name: 'colonia', title: 'Colonia', type: 'text', validate: 'required'},
            {name: 'calle', title: 'Calle', type: 'text', validate: 'required'},
            {name: 'numero', title: '#', type: 'text', validate: 'required'},
            {type: "control", itemTemplate:function(value, item){
                var $result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
                var $myButton = $('<a title="Ver Mapa"><i class="far fa-map"></i></a>');
                $myButton.click(function(){
                    console.log(item);
                    var direccion = item.calle + " " + item.numero + " " + 'colonia ' + item.colonia + ', ' + item.municipio + ", " + item.estado;
                    $('#myLargeModalLabel').html('<p>Clave: ' + item.clave + '</p><p>Nombre: ' + item.nombre + '</p><p>Dirección: '+direccion+'</p>');
                    $(".bd-example-modal-lg").modal({'show':true});
                    var myLatLng = {lat: parseFloat(item.latitud), lng: parseFloat(item.longitud)};
                    map = new google.maps.Map(document.getElementById('map'), {
                        zoom: 15,
                        center: myLatLng
                    });
                    var marker = new google.maps.Marker({
                        position: myLatLng,
                        map: map,
                        title: 'Hello World!'
                    });


                    var contentString = '<div id="content">'+
                                '<div id="siteNotice">'+
                                '</div>'+
                                '<h2 id="firstHeading" class="firstHeading">'+item.clave+' '+item.nombre+'</h2>'+
                                '<div id="bodyContent"><div id="galeria"></div>'+
                                '</div>'+
                                '</div>';
                    var infowindow = new google.maps.InfoWindow({
                        content: contentString
                    });
                    marker.addListener('click', function() {
                        infowindow.open(map, marker);
                        var destino = site_url + '/construccion/galeria/'+item.clave;
                        data_ajax(destino, null, '#galeria');
                    });

                    destino = site_url + '/construccion/caracteristicas/' + item.clave + '/0';
                    data_ajax(destino, null, '#area_caracteristicas2');
                });
                $myButton.tooltip();
                $gButton = $('<a title="Generar galeria"><i class="fas fa-images"></i></a>');
                $gButton.click(function(){
                    var destino = site_url + '/construccion/crear_galeria/'+item.clave + '/' + item.latitud + '/' + item.longitud;
                    data_ajax(destino);
                });
                $gButton.tooltip();
                $caracButton = $('<a title="Asignar caracteristicas"><i class="fas fa-list-ul"></i></a>');
                $caracButton.click(function(){
                    var destino = site_url + '/construccion/caracteristicas/' + item.clave;
                    data_ajax(destino, null, '#area_caracteristicas');
                    $('#carac_clave').html(item.clave);
                    $(".bd-caracteristicas-modal-lg").modal({'show':true});
                });
                $caracButton.tooltip();
                // return $myButton.add($('<span> </span>')).add($gButton).add($('<span> </span>')).add($result);
                return $myButton.add($('<span> </span>')).add($caracButton).add($('<span> </span>')).add($result);
            }}
        ]
    }).data("JSGrid");

    var origFinishInsert = jsGrid.loadStrategies.DirectLoadingStrategy.prototype.finishInsert;
    jsGrid.loadStrategies.DirectLoadingStrategy.prototype.finishInsert = function (insertedItem) {
        if (!this._grid.insertSuccess) { // define insertFailed on done of delete ajax request in insertFailed of controller
            return;
        }
        origFinishInsert.apply(this, arguments);
    };

    $("#pager").on("change", function() {
        var page = parseInt($(this).val(), 10);
        $("#lista_registros").jsGrid("option", "pageSize", page);
    });
}

var map;
var geocoder;

function initMap() {
    geocoder = new google.maps.Geocoder();

    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: -34.397, lng: 150.644},
        zoom: 8
    });
}


function getLatLong(direccion) {
    var deferred = $.Deferred();
    geocoder.geocode( { 'address': direccion}, function(results, status) {
        if (status == 'OK') {
            console.log(results);
            console.log(results[0].geometry.location.lat());
            deferred.resolve(results[0].geometry.location);
        } else {
            alert('Geocode was not successful for the following reason: ' + status);
            deferred.reject(status);
        }
    });
    return deferred.promise();
}

function agregar_caracteristica(){
    var c = document.getElementById('textinput').value;
    document.getElementById('textinput').value = '';
    $nueva = $('<div class="col-md-4"><i class="fas fa-times-circle" onclick="quitar_caracteristica(this)"></i><p class="item-caracteristica">'+c + '</p></div>');
    $('#area_caracteristicas').append($nueva);
}

function quitar_caracteristica(item){
    // console.log(item);
    item.parentNode.remove();
}
