/*$(document).ready(function () {
 $('[data-toggle="tooltip"]').tooltip(); //Llamada a tooltip
 });*/

/**
 *	Método que muestra una imagen (gif animado) que indica que algo esta cargando
 *	@return	string	Contenedor e imagen del cargador.
 */
function create_loader() {
    return '<img src="' + img_url_loader + '" alt="Cargando..." title="Cargando..." />';
//    return '<div id="ajax_loader" align="center" style="padding-top:200px; padding-bottom:200px;">\n\
//<img src="' + img_url_loader + '" alt="Cargando..." title="Cargando..." />\n\
//</div>';
}

/**
 *	Método que remueve el contenedor e imagen de cargando
 */
function remove_loader() {
    $("#ajax_loader").remove();
}
function mostrar_loader() {
    $('#overlay').fadeIn('slow', function () {

    });
}

function ocultar_loader() {
    $('#overlay').fadeOut('slow', function () {

    });
}

/**	Método que muestra un mensaje con formato de alertas de boostrap
 * @param	string	message 	Mensaje que será mostrado
 * @param 	string 	tipo 		Posibles valores('success','info','warning','danger')
 */
function html_message(message, tipo) {
    tipo = (typeof (tipo) === "undefined") ? 'danger' : tipo;
    return "<div class='alert alert-" + tipo + "' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>" + message + "</div>";
}

/*
 * @author  ???
 * @modified_by DPérez
 * @param url para conexión ajax
 * @param id html del formulario donde se obtienen los datos a enviar en ajax
 * @param id html del elemento que contendrá los datos del resultado
 * @param función que se ejecutará cuando el ajax es correcto y se tienen datos
 * @param is_jason para indicar que el resultado esperado es una cadena en formato json
 * @param parametros para la función de callback, opcional
 * @returns none
 * @modificada - Christian Garcia 9 de junio 2017
 */
function data_ajax(path, form_recurso, elemento_resultado, callback, is_json, parametros) {
    var dataSend = $(form_recurso).serialize();
    $.ajax({
        url: path,
        data: dataSend,
        method: 'POST',
        beforeSend: function (xhr) {
//            $(elemento_resultado).html(create_loader());
            mostrar_loader();
        }
    }).done(function (response) {
        var html = response;
        var json = null;
        if (typeof is_json !== 'undefined' && is_json) {
            json = response;
            //json = JSON.parse(response);
            // console.log(json);
            if (typeof json.html !== 'undefined') {
                html = json.html;
            }
        }
        if (typeof callback !== 'undefined' && typeof callback === 'function') {
            if (typeof parametros !== 'undefined') {
                if (is_json !== 'undefined' && is_json) {
                    parametros.object = json;
                }
                $(elemento_resultado).html(html).promise().done(callback(parametros));
            } else {
                $(elemento_resultado).html(html).promise().done(callback());
            }
        } else {
            $(elemento_resultado).html(html);
        }
    }).fail(function (jqXHR, textStatus) {
        $(elemento_resultado).html("Ocurrió un error durante el proceso, inténtelo más tarde.");
    }).always(function () {
        remove_loader();
        ocultar_loader();
    });
}

function selectItemByValue(elmnt_name, value) {
    var elmnt = document.getElementById(elmnt_name);
    for (var i = 0; i < elmnt.options.length; i++)
    {
        if (elmnt.options[i].value === value) {
            elmnt.selectedIndex = i;
//            break;
        }
    }
}

function removeOptions(selectbox){
    var i;
    for(i=selectbox.options.length-1;i>=0;i--)
    {
        selectbox.remove(i);
    }
    option = document.createElement("option");
    option.value = '';
    option.text = 'Seleccionar opción...';
    selectbox.add(option);
}
