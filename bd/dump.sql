create table construcciones(
clave varchar(11) not null,
nombre varchar(255) not null,
estado varchar(255) not null,
municipio varchar(255) not null,  -- delegacion
colonia varchar(255) not null,
calle varchar(255) not null,
numero varchar(5) not null,
latitud float not null,
longitud float not null,
caracteristicas text,
primary key(clave)
);

drop table galeria;
create table galeria(
clave varchar(11) not null,
id_imagen varchar(100) not null,
name varchar(250) not null,
foto varchar(300) not null,
primary key(clave, id_imagen),
foreign key(clave) references construcciones(clave)
);

INSERT INTO Users (FirstName,LastName, Email, Password) VALUES ('Christian', 'Garcia', 'chrigarc@ciencias.unam.mx', 'b36528f97f37758ddccfc3c5c5f6bf23');
